#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>

void buatTim(int bek, int gel, int str){ 
    for (int i=0; i<4; i++) {
        pid_t child_id1 = fork();
        
        if (child_id1 < 0) {
            exit(EXIT_FAILURE);
        }
        else if (child_id1 == 0) {
            if(i == 0){
                char *argv[] = {"/bin/sh", "-c", "ls Penyerang > penyerang.txt", NULL};
                execv("/bin/sh", argv);
            }
            else if(i == 1){
                char *argv[] = {"/bin/sh", "-c", "ls Gelandang > gelandang.txt", NULL};
                execv("/bin/sh", argv);
            }
            else if(i == 2){
                char *argv[] = {"/bin/sh", "-c", "ls Bek > bek.txt", NULL};
                execv("/bin/sh", argv);
            }
            else if(i == 3){
                char *argv[] = {"/bin/sh", "-c", "ls Kiper > kiper.txt", NULL};
                execv("/bin/sh", argv);
            }
        }
        else {
            int status;
            while(wait(&status) > 0);
        }
    }

    for (int i=0; i<4; i++) {
        pid_t child_id2 = fork();

        if (child_id2 < 0) {
            exit(EXIT_FAILURE);
        }
        else if (child_id2 == 0) {
            if(i == 0){
                char *argv[] = {"sort", "-t_", "-k", "3", "-r", "penyerang.txt", "-o", "penyerang.txt", NULL};
                execv("/bin/sort", argv);
            }
            else if(i == 1){
                char *argv[] = {"sort", "-t_", "-k", "3", "-r", "gelandang.txt", "-o", "gelandang.txt", NULL};
                execv("/bin/sort", argv);
            }
            else if(i == 2){
                char *argv[] = {"sort", "-t_", "-k", "3", "-r", "bek.txt", "-o", "bek.txt", NULL};
                execv("/bin/sort", argv);
            }
            else if(i == 3){
                char *argv[] = {"sort", "-t_", "-k", "3", "-r", "kiper.txt", "-o", "kiper.txt", NULL};
                execv("/bin/sort", argv);
            }
        }
        else {
            int status;
            while(wait(&status) > 0);
        }
    }

    for (int i=0; i<4; i++) {
        pid_t child_id3 = fork();

        if (child_id3 < 0) {
            exit(EXIT_FAILURE);
        }
        else if (child_id3 == 0) {
            if(i == 3){
                char command[100];
                sprintf(command, "head -n %d penyerang.txt >> /home/jagircom/Formasi_%d_%d_%d.txt", str, bek, gel, str);
                char *argv[] = {"/bin/sh", "-c", command, NULL};
                execv("/bin/sh", argv);
            }            
            else if(i == 2){
                char command[100];
                sprintf(command, "head -n %d gelandang.txt >> /home/jagircom/Formasi_%d_%d_%d.txt", gel, bek, gel, str);
                char *argv[] = {"/bin/sh", "-c", command, NULL};
                execv("/bin/sh", argv);
            }
            else if(i == 1){
                char command[100];
                sprintf(command, "head -n %d bek.txt >> /home/jagircom/Formasi_%d_%d_%d.txt", bek, bek, gel, str);
                char *argv[] = {"/bin/sh", "-c", command, NULL};
                execv("/bin/sh", argv);
            }
            else if(i == 0){
                char command[100];
                sprintf(command, "head -n 1 kiper.txt >> /home/jagircom/Formasi_%d_%d_%d.txt", bek, gel, str);
                char *argv[] = {"/bin/sh", "-c", command, NULL};
                execv("/bin/sh", argv);
            }
        }
        else {
            int status;
            while(wait(&status) > 0);
        }
    }
}

void positionFolder(){
    pid_t pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    }
    else if (pid == 0){
        char *argv[] = {"mkdir", "Penyerang", "Gelandang", "Bek", "Kiper", NULL};
        execv("/bin/mkdir", argv);
    }
    else {
        int status;
        while(wait(&status) > 0);
    }

    for (int i=0; i<4; i++) {
        pid_t child_id = fork();

        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }
        else if (child_id == 0) {
            if(i == 0){
                char *argv[] = {"find", "players", "-name", "*Penyerang*", "-type", "f", "-exec", "cp", "{}", "Penyerang", ";", NULL};
                execv("/bin/find", argv);
            }
            else if(i == 1){
                char *argv[] = {"find", "players", "-name", "*Gelandang*", "-type", "f", "-exec", "cp", "{}", "Gelandang", ";", NULL};
                execv("/bin/find", argv);
            }
            else if(i == 2){
                char *argv[] = {"find", "players", "-name", "*Bek*", "-type", "f", "-exec", "cp", "{}", "Bek", ";", NULL};
                execv("/bin/find", argv);
            }
            else if(i == 3){
                char *argv[] = {"find", "players", "-name", "*Kiper*", "-type", "f", "-exec", "cp", "{}", "Kiper", ";", NULL};
                execv("/bin/find", argv);
            }
        }
        else {
            int status;
            while(wait(&status) > 0);
        }
    }

}

void removeManUtd(){
    pid_t child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *args[] = {"find", "players", "-type", "f", "!", "-name", "*_ManUtd_*", "-exec", "rm", "{}", "+", NULL};
        execvp("find", args);
    }
    else {
        int status;
        while(wait(&status) > 0);
    }
}

void removeZip(){
    pid_t child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"rm", "players.zip", NULL};
        execv("/bin/rm", argv);
    }
    else {
        int status;
        while(wait(&status) > 0);
    }
}

void extractZip(){
    pid_t child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"unzip", "players.zip", NULL};
        execv("/bin/unzip", argv);
    }
    else {
        int status;
        while(wait(&status) > 0);
        removeZip();
    }
}

void downloadZip(){
    pid_t child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"wget", "-O", "players.zip", "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
        execv("/bin/wget", argv);
    }
    else {
        int status;
        while(wait(&status) > 0);
        extractZip();
    }
}

int main(){
    downloadZip();
    removeManUtd();
    positionFolder();
    buatTim(4, 4, 2);
    return 0;
}