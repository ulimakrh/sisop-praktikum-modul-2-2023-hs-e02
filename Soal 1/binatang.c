#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>

void zip2(){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"zip", "-r", "-m", "HewanAmphibi.zip", "HewanAmphibi/", NULL};
        execv("/bin/zip", argv);
    }
    else {
        int status;
        while((wait(&status)) > 0);
        char *argv[] = {"zip", "-r", "-m", "HewanAir.zip", "HewanAir/", NULL};
        execv("/bin/zip", argv);
    }
}

void zip1(){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"zip", "-r", "-m", "HewanDarat.zip", "HewanDarat/", NULL};
        execv("/bin/zip", argv);
    }
    else {
        int status;
        while((wait(&status)) > 0);
        zip2();
    }
}

int main(){
    system("wget -O binatang.zip \"https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq\"");
    pid_t child_id;
    child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"unzip", "binatang.zip", NULL};
        execv("/bin/unzip", argv);
    }
    else {
        int status;
        while((wait(&status)) > 0);
        system("ls | grep -E '(darat|amphibi|air).jpg' | shuf -n 1");
        system("mkdir -p HewanDarat HewanAmphibi HewanAir");
        system("mv *darat.jpg HewanDarat/");
        system("mv *amphibi.jpg HewanAmphibi/");
        system("mv *air.jpg HewanAir/");
        zip1();
    }
}