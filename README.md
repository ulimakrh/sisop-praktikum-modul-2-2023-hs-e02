# Praktikum 2 Sistem Operasi 2023

Kelompok E02

1. 5025211008 - Muhammad Razan Athallah
2. 5025211232 - Ulima Kaltsum Rizky Hibatullah
3. 5025211137 - Kalyana Putri Al Kanza

## Soal 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

a. Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

b. Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

c. Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

d. Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

	Catatan :
	untuk melakukan zip dan unzip tidak boleh menggunakan system

### Penyelesaian
Soal ini meminta kita untuk mendownload gambar terlebih dahulu dari Google Drive, lalu meng-unzip folder yang sudah didownload dan memilih acak file gambar di dalamnya. Gambar-gambar hewan tersebut kemudian dipilah ke direktori sesuai dengan habitatnya, kemudian folder di-zip kembali.

Jadi langkah pertama adalah program mengunduh file binatang.zip dari link Google Drive menggunakan perintah "system" seperti di bawah ini.

```c
int main(){
    system("wget -O binatang.zip \"https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq\"");
```

Berikut adalah proses mengunduh file binatang.zip:
![Screenshot_2023-04-08_204453](/uploads/00b3ffcea411380bd4e6f495a90f8fe6/Screenshot_2023-04-08_204453.png)

Lalu dengan fungsi `fork()` akan dihasilkan dua proses yaitu child process dan parent process. Pada child process dilakukan unzip dari file binatang.zip menggunakan perintah `execv()`. Array argv digunakan untuk menampung daftar argumen yang diperlukan untuk menjalankan perintah unzip. Argumen terakhir harus diatur sebagai NULL untuk menunjukkan akhir dari daftar argumen. Jika proses `execv()` berhasil dijalankan, maka child process akan keluar dengan status EXIT_SUCCESS. Namun, jika proses `execv()` gagal, maka program akan keluar dengan status EXIT_FAILURE.


```c
pid_t child_id;
    child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"unzip", "binatang.zip", NULL};
        execv("/bin/unzip", argv);
    }
```

Berikut adalah hasil unzip dari file binatang.zip:
![Screenshot_2023-04-08_204724](/uploads/f4f2a3ec159f7ad04c54821d5ab917f7/Screenshot_2023-04-08_204724.png)

Sementara itu, parent process menunggu child process selesai menggunakan perintah "wait" dan setelah selesai mengekstrak file zip, program mencari file gambar binatang yang berhubungan dengan habitatnya menggunakan perintah "ls | grep -E '(darat|amphibi|air).jpg' | shuf -n 1". `grep -E` akan mencari file-file yang memiliki nama file dengan pola "darat.jpg", "amphibi.jpg", atau "air.jpg" dan `shuf -n 1` akan mengambil secara acak satu baris (file) dari hasil pencarian yang diberikan oleh perintah grep. Sehingga, perintah `ls | grep -E '(darat|amphibi|air).jpg' | shuf -n 1` akan menampilkan nama file gambar secara acak, yang memiliki nama file yang cocok dengan pola "darat.jpg", "amphibi.jpg", atau "air.jpg" pada direktori saat ini.

```c
  else {
        int status;
        while((wait(&status)) > 0);
        system("ls | grep -E '(darat|amphibi|air).jpg' | shuf -n 1");
```

Berikut adalah beberapa hasil pengambilan 1 file acak dari beberapa percobaan:
![Screenshot_2023-04-08_205426](/uploads/e2d6d0e41b882ced0cdd8d284a761308/Screenshot_2023-04-08_205426.png)
![Screenshot_2023-04-08_205452](/uploads/d91fc3dae418584f0b06507b5f23e226/Screenshot_2023-04-08_205452.png)
![Screenshot_2023-04-08_205543](/uploads/80c5c289f5a288577093c875d67e66b1/Screenshot_2023-04-08_205543.png)

Lalu kita membuat direktori untuk setiap habitat menggunakan perintah `mkdir` dan memindahkan file gambar binatang ke direktori yang sesuai dengan habitatnya menggunakan perintah `mv`.

```c
system("mkdir -p HewanDarat HewanAmphibi HewanAir");
        system("mv *darat.jpg HewanDarat/");
        system("mv *amphibi.jpg HewanAmphibi/");
        system("mv *air.jpg HewanAir/");
        zip1();
    }
}
```
Berikut adalah proses mv file-file binatang ke folder habitat masing-masing:
![Screenshot_2023-04-08_210028](/uploads/f88fc78a944a672119b859b8ef48cbef/Screenshot_2023-04-08_210028.png)

Fungsi `zip1()` di bawah ini berfungsi untuk menjalankan program zip dan melakukan kompresi folder HewanDarat/ menjadi file HewanDarat.zip. Lalu, ia akan menghapus folder asli setelah berhasil dikompresi. Setelah proses kompresinya selesai, fungsi zip2() akan dijalankan.
```c
void zip1(){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"zip", "-r", "-m", "HewanDarat.zip", "HewanDarat/", NULL};
        execv("/bin/zip", argv);
    }
    else {
        int status;
        while((wait(&status)) > 0);
        zip2();
    }
}
```
Sementara itu, kompresi untuk folder HewanAmphibi/ dan HewanAir/ dilakukan pada fungsi `zip2()`. Fungsi ini nantinya juga akan menggunakan` fork()`untuk membuat proses baru yang akan menjalankan program zip dan hasilnya akan disimpan dalam variabel `child_id`. Jika `child_id` = 0, maka program yang akan dijalankan adalah zip `-r -m` HewanAmphibi.zip HewanAmphibi/. Di sini, zip adalah program yang akan dijalankan, -r adalah argumen yang menandakan kompresi dilakukan secara rekursif (semua file dan folder di dalam folder yang akan dikompresi akan ikut terkompresi) dan tanpa ini, zip hanya akan mengompresi file/folder yang berada di dalam folder yang ditentukan tanpa memasukkan file/folder di dalamnya. "-m" adalah argumen yang menandakan bahwa file/folder asli akan dihapus setelah berhasil dikompresi. Perintah tersebut akan dijalankan menggunakan fungsi execv(). Jika child_id =/ 0, maka program akan menunggu child process selesai dijalankan dengan menggunakan perintah wait(&status). Setelah itu, program zip dijalankan kembali, namun untuk folder HewanAir.zip HewanAir/. 

```c
void zip2(){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"zip", "-r", "-m", "HewanAmphibi.zip", "HewanAmphibi/", NULL};
        execv("/bin/zip", argv);
    }
    else {
        int status;
        while((wait(&status)) > 0);
        char *argv[] = {"zip", "-r", "-m", "HewanAir.zip", "HewanAir/", NULL};
        execv("/bin/zip", argv);
    }
}
```
Pada dasarnya, fungsi zip2() ini berfungsi untuk melakukan kompresi pada dua folder, yaitu HewanAmphibi/ dan HewanAir/ secara bersamaan. Proses kompresi akan dilakukan secara rekursif dan file/folder asli akan dihapus setelah berhasil dikompresi. Setelah proses kompresi kedua folder selesai, program akan berakhir.

Berikut adalah isi hasil zip dari masing-masing folder habitat:
![Screenshot_2023-04-08_205751](/uploads/65f0d2d6109f7feac01bcdbc20b8fd51/Screenshot_2023-04-08_205751.png)
![Screenshot_2023-04-08_205830](/uploads/e6c44d5fa3770b79533d88bc1712564b/Screenshot_2023-04-08_205830.png)
![Screenshot_2023-04-08_205805](/uploads/2a161e5732a5ac4265d6e7fb0cd259e1/Screenshot_2023-04-08_205805.png)

## Soal 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

a. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

b. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

c. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

d. Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

e. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

	Catatan :
	- Tidak boleh menggunakan system()
	- Proses berjalan secara daemon
	- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

### Penyelesaian
Soal ini meminta kita untuk membuat program C yang akan membuat folder khusus berisi program C yang setiap 30 detik akan membuat folder baru dan setiap folder akan diisi dengan 15 gambar dimana tiap gambar di-download setiap 5 detik. Setelah sebuah folder terisi 15 gambar, folder tersebut akan di-zip dan folder aslinya akan dihapus. Program harus berjalan dalam mode daemon dan harus berhenti ketika program killer dijalankan atau program utama akan berhenti tetapi proses di setiap folder yang masih berjalan akan berjalan sampai selesai. Selain itu, program juga harus dapat membuat program "killer" untuk menterminasi semua operasi program tersebut dan akan menghapus dirinya sendiri.

Kode utama di dalam main() akan melakukan `fork()` dua kali untuk membuat dua child process yang nantinya akan digunakan sebagai dua daemon berbeda. Sementara itu, parent process akan di return 0.

Child process pertama akan masuk ke fungsi `createKillerAndFolder()` dengan passing argumen argv. Sedangkan child process kedua akan masuk ke fungsi `findFolder()`.

```c
int main(int argc, char *argv[]) {
    pid_t pid1, pid2;

    pid1 = fork();
    if (pid1 == 0) {
        createKillerAndFolder(argv);
    } else if (pid1 < 0) {
        exit(EXIT_FAILURE);
    }

    pid2 = fork();
    if (pid2 == 0) {
        findFolder();
    } else if (pid2 < 0) {
        exit(EXIT_FAILURE);
    }

    return 0;
}
```

Fungsi `createKillerAndFolder` berfungsi untuk membuat "killer" dan  membuat folder secara terus menerus dalam interval 30 detik. Fungsi untuk membuat folder akan dijalankan sebagai sebuah daemon agar bisa berjalan secara terus menerus tanpa interupsi dari pengguna. Pertama-tama, fungsi ini melakukan `fork()` untuk membuat proses baru. Jika hasilnya < 0, maka program akan keluar dan mengembalikan nilai EXIT_FAILURE yang menunjukkan bahwa ada kesalahan dalam pembuatan proses baru. Proses baru akan melanjutkan eksekusi kode apabila nilai pid > 0.
```c
void createKillerAndFolder(char *argv[]) {
    pid_t pid = fork(), sid;
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }
```
Pada proses selanjutnya, program akan mengatur file permission dengan menggunakan `umask(0)`. Kemudian, program akan membuat session baru dengan menggunakan `setsid()`. Lalu, program akan mengubah working directory ke root directory dengan menggunakan `chdir("/")`. Jika `chdir("/")` dan `setsid()` menghasilkan nilai negatif, maka program akan keluar dan mengembalikan nilai EXIT_FAILURE. Setelah itu, program akan menutup file descriptor `STDIN_FILENO`, `STDOUT_FILENO`, dan `STDERR_FILENO` dengan menggunakan `close()` untuk menghilangkan bagian yang tidak dibutuhkan.
```c
    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
```
Fungsi `createKiller()` kemudian dipanggil dengan memasukkan argumen `argv` dan `getpid()` Fungsi `createKiller()` digunakan untuk membuat file dengan nama killer.sh yang berfungsi untuk membunuh proses yang sedang berjalan. Terakhir, loop `while` akan terus melakukan pemanggilan fungsi createFolder() setiap 30 detik dengan menggunakan sleep(30). Fungsi createFolder() bertanggung jawab untuk membuat folder baru di dalam working directory.
```c
    createKiller(argv, getpid());
    while (1) {
        createFolder();
        sleep(30);
    }
}
```

Fungsi `createKiller()` akan membuat program "killer" yang digunakan untuk menghentikan program utama yang sedang berjalan. Terdapat 2 opsi dalam pembuatan program "killer", yaitu opsi `-a` dan `-b`, yang ditentukan melalui argumen yang diberikan pada saat menjalankan program lukisan.

Jika argumen pertama adalah `-a`, maka fungsi akan membuat program "killer" yang akan menghentikan program utama dengan menggunakan perintah pkill pada terminal. Program "killer" ini akan otomatis dihapus setelah program utama dihentikan.
```c
void createKiller(char *argv[], int pid){
    if (!strcmp(argv[1], "-a")){
        FILE *fp;
        fp = fopen("/home/razanathallah/Documents/SISOP/M2/killer.c", "w");
        
        fprintf(fp, 
            "#include <stdlib.h>\n"
            "#include <sys/types.h>\n"
            "#include <unistd.h>\n"
            "#include <stdio.h>\n"
            "#include <wait.h>\n"

            "void removeKiller(){\n"
                "pid_t child_id = fork();\n"
                "if (child_id < 0){\n"
                    "exit(EXIT_FAILURE);\n"
                "}\n"
                "else if (child_id == 0){\n"
                    "char *args[] = {\"rm\", \"killer.c\", NULL};\n"
                    "execv(\"/bin/rm\", args);\n"
                "}\n"
                "else {\n"
                    "char *args[] = {\"rm\", \"killer\", NULL};\n"
                    "execv(\"/bin/rm\", args);\n"
                "}\n"
            "}\n"

            "int main(){\n"
                "pid_t child_id = fork();\n"
                "if (child_id < 0){\n"
                    "exit(EXIT_FAILURE);\n"
                "}\n"
                "else if (child_id == 0){\n"
                    "char *args[] = {\"pkill\", \"lukisan\", NULL};\n"
                    "execv(\"/bin/pkill\", args);\n"
                "}\n"
                "else {\n"
                    "removeKiller();\n"
                "}\n"
            "}\n"
        );
        fclose(fp);
        compileKiller();
    }
```
Sedangkan jika argumen pertama adalah `-b`, maka fungsi akan membuat program "killer" yang akan menghentikan program utama dengan menggunakan perintah kill -9 dengan pid daemon pembuatan folder. Program "killer" ini juga akan otomatis dihapus setelah program utama dihentikan.
```c
else if (!strcmp(argv[1], "-b")){
        FILE *fp;
        fp = fopen("/home/razanathallah/Documents/SISOP/M2/killer.c", "w");
        
        fprintf(fp, 
            "#include <stdlib.h>\n"
            "#include <sys/types.h>\n"
            "#include <unistd.h>\n"
            "#include <stdio.h>\n"
            "#include <wait.h>\n"

            "void removeKiller(){\n"
                "pid_t child_id = fork();\n"
                "if (child_id < 0){\n"
                    "exit(EXIT_FAILURE);\n"
                "}\n"
                "else if (child_id == 0){\n"
                    "char *args[] = {\"rm\", \"killer.c\", NULL};\n"
                    "execv(\"/bin/rm\", args);\n"
                "}\n"
                "else {\n"
                    "char *args[] = {\"rm\", \"killer\", NULL};\n"
                    "execv(\"/bin/rm\", args);\n"
                "}\n"
            "}\n"

            "int main(){\n"
                "pid_t child_id = fork();\n"
                "if (child_id < 0){\n"
                    "exit(EXIT_FAILURE);\n"
                "}\n"
                "else if (child_id == 0){\n"
                    "char strpid[10];\n"
                    "sprintf(strpid, \"%%d\", %d);\n"
                    "char *args[] = {\"kill\", \"-9\", strpid, NULL};\n"
                    "execv(\"/bin/kill\", args);\n"
                "}\n"
                "else {\n"
                    "removeKiller();\n"
                "}\n"
            "}\n",
            pid
        );
```
Lalu, fungsi ini akan memanggil fungsi `compileKiller()`, yang digunakan untuk mengkompilasi program "killer" yang baru saja dibuat.
```c
fclose(fp);
        compileKiller();
    }
}
```

`compileKiller()` merupakan fungsi untuk mengkompilasi kode pembuat file killer. Fungsi ini akan menghasilkan file killer pada direktori yang ada. 
Kode ini merupakan sebuah fungsi bernama "compileKiller" yang bertujuan untuk mengkompilasi file "killer.c" menjadi sebuah executable file "killer". Proses pengkompilasian dilakukan dengan menggunakan perintah `gcc` yang dijalankan pada shell melalui fungsi `execv` yang digunakan untuk mengeksekusi perintah `gcc` dengan argumen yang telah ditentukan dalam array `args`.

Pertama, kode akan melakukan fork untuk membuat sebuah child process. Jika gagal, maka program akan keluar dan mengembalikan nilai EXIT_FAILURE. Jika proses fork berhasil, maka child process akan mengeksekusi perintah untuk mengkompilasi file "killer.c" menjadi executable file "killer". Argumen-argumen yang digunakan adalah lokasi file yang akan dikompilasi, `-o` untuk memberikan nama output file, nama output file dan NULL sebagai argumen terakhir. Setelah proses kompilasi selesai, maka file executable "killer" akan tersedia dan dapat dieksekusi untuk menghentikan proses.
```c
void compileKiller(){
    pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *args[] = {"gcc", "/home/razanathallah/Documents/SISOP/M2/killer.c", "-o", "/home/razanathallah/Documents/SISOP/M2/killer", NULL};
        execv("/bin/gcc", args);
    }
}
```

Setelah killer dibuat, maka dilanjutkan dengan looping fungsi `createFolder` yang merupakan fungsi dengan tujuan untuk membuat sebuah folder baru dengan nama sesuai dengan waktu saat fungsi tersebut dipanggil. Fungsi ini akan membuat sebuah child process baru menggunakan fungsi `fork()`. Jika `fork()` < 0, maka program akan keluar dengan status failure. Jika nilai kembaliannya = 0, maka program sedang berada dalam child process. Pada child process ini, fungsi `localtime` akan digunakan untuk mendapatkan waktu saat ini, kemudian waktu tersebut akan digunakan untuk membuat nama folder baru dalam format YYYY-MM-DD_HH:MM:SS. Setelah itu, program akan menggunakan fungsi `execv` untuk menjalankan command `mkdir` pada terminal menggunakan parameter nama folder yang telah dibuat sebelumnya sebagai argumen. Parameter pertama pada `execv` adalah path dari command yang ingin dijalankan (dalam hal ini /bin/mkdir), dan parameter kedua adalah array string yang berisi argumen-argumen yang ingin diberikan pada command tersebut. Dalam hal ini, argumen yang diberikan adalah nama folder yang telah dibuat sebelumnya.

Dengan demikian, jika fungsi `createFolder` dipanggil, maka akan terbuat sebuah folder baru dengan nama yang mengacu pada waktu saat ini.
```c
void createFolder(){
    pid_t pid = fork();
    if (pid < 0){
        exit(EXIT_FAILURE);
    }
    else if (pid == 0){
        char name[120];
        time_t t = time(NULL);
        struct tm *current = localtime(&t);
        sprintf(name, "/home/razanathallah/Documents/SISOP/M2/%04d-%02d-%02d_%02d:%02d:%02d", current->tm_year + 1900, 
                current->tm_mon + 1, current->tm_mday, current->tm_hour, current->tm_min, current->tm_sec);  
        char *args[] = {"mkdir", name, NULL};
        execv("/bin/mkdir", args);
    }
}
```

Pada child process kedua akan dipanggil fungsi `findFolder` adalah sebuah fungsi yang tidak mengembalikan nilai dan tidak memiliki parameter. Fungsi ini pertama-tama akan melakukan fork pada proses utama, kemudian melakukan beberapa pengaturan dasar pada proses baru yang dihasilkan, seperti pengaturan session id, perubahan working directory, dan menutup file descriptor standar.
```c
void findFolder() {
    pid_t pid = fork(), sid;
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
```
Setelah itu, program akan masuk ke dalam loop `while` yang akan berjalan selama program berjalan. Pada setiap iterasi loop, program akan membuka folder yang ditentukan pada variabel path menggunakan fungsi `opendir` dan membaca isi folder menggunakan `readdir`. Program akan memeriksa setiap file atau folder yang ditemukan dengan melakukan pengecekan apakah itu direktori menggunakan fungsi `stat` dan `S_ISDIR`. Jika itu direktori, maka program akan menggunakan fungsi `countFiles` untuk menghitung jumlah file dalam folder tersebut dan memeriksa apakah jumlah file sudah mencapai batas tertentu. Jika belum mencapai batas, maka program akan menggunakan fungsi `downloadImage` untuk melakukan operasi download pada folder tersebut. Namun jika batas sudah tercapai, program akan menggunakan fungsi `zipFolder` untuk melakukan operasi zip pada folder tersebut. Setelah selesai melakukan melakukan pengecekan pada folder, program akan melakukan `sleep` selama 5 detik sebelum melakukan pengecekan pada folder berikutnya pada iterasi selanjutnya dari loop.
```c
while (1) {
        DIR *dp;
        struct dirent *ep;
        char *path = "/home/razanathallah/Documents/SISOP/M2";
        dp = opendir(path);
        struct stat sb;

        if (dp != NULL) {
            while ((ep = readdir (dp))) {
                if (!strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..")) continue;

                char folderpath[300];
                sprintf(folderpath, "%s/%s", path, ep->d_name);
                if (stat(folderpath, &sb) == -1) continue;

                if (S_ISDIR(sb.st_mode)){
                    int count = countFiles(folderpath);
                    if (count < 15) downloadImage(folderpath);
                    else zipFolder(path, ep->d_name);
                }
            }
            closedir (dp);
        }
        sleep(5);
    }
}
```

Fungsi `zipFolder`merupakan fungsi untuk melakukan kompresi pada suatu folder dengan nama tertentu. Fungsi ini akan membuat sebuah proses baru untuk menjalankan command `zip -r -m <name> <name>` pada direktori dengan path yang diberikan. Fungsi ini menerima dua parameter, yaitu path yang berisi path direktori dari folder yang ingin di-zip, dan name berisi nama file zip yang akan dihasilkan. Setelah menerima kedua parameter tersebut, fungsi ini membuat sebuah child process menggunakan fungsi `fork()`. Kemudian, dilakukan pengecekan apakah child process berhasil dibuat atau tidak. Jika gagal, maka program akan keluar dengan EXIT_FAILURE.

Jika berhasil, child process akan melakukan perubahan direktori ke path yang telah diberikan menggunakan fungsi `chdir()`. Selanjutnya, `args` diinisialisasi dengan isi perintah-perintah untuk menjalankan perintah `zip` pada terminal. Dalam kasus ini, perintah-perintah tersebut adalah `zip`, `-r`, `-m`, name, name, dan NULL. Kemudian, perintah-perintah tersebut dijalankan menggunakan fungsi `execv()` yang menerima dua parameter, yaitu path dari executable file yang ingin dijalankan dan `args` yang berisi argumen untuk executable file tersebut. Executable file yang ingin dijalankan adalah /bin/zip (path dari program zip).
```c
void zipFolder(char path[], char name[]){
    pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        chdir(path);
        char *args[] = {"zip", "-r", "-m", name, name, NULL};
        execv("/bin/zip", args);
    }
}
```
Fungsi `downloadImage` digunakan untuk mengunduh gambar dari link web dan menyimpannya dalam folder yang ditentukan oleh user. Fungsi ini menggunakan fork untuk membuat proses baru. Saat proses baru berhasil dibuat, maka akan dilakukan unduhan gambar menggunakan `wget` dengan menggunakan perintah `execv`.

Fungsi `sprintf` akan memformat string yang akan digunakan sebagai nama file yang akan disimpan, dan juga untuk menentukan URL yang akan diunduh. Sementara struct `tm` digunakan untuk memperoleh waktu saat ini dan memformatnya ke dalam string. Hal ini dilakukan untuk memberi unique name untuk setiap gambar yang diunduh Ketika proses berhasil dibuat, kode akan mengunduh gambar dengan menggunakan `wget` pada URL yang dihasilkan dengan `sprintf`, dan kemudian menyimpannya dengan nama file yang dihasilkan.

Pada kode ini, `-q` digunakan untuk menonaktifkan output pesan dari `wget`, dan `-O` digunakan untuk menentukan nama file output.
```c
void downloadImage(char folderpath[]){
    pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char name[400], url[100];
        time_t t = time(NULL);
        struct tm *current = localtime(&t);

        sprintf(name, "%s/%04d-%02d-%02d_%02d:%02d:%02d", folderpath, current->tm_year + 1900, 
                current->tm_mon + 1, current->tm_mday, current->tm_hour, current->tm_min, current->tm_sec);  
        sprintf(url, "https://picsum.photos/%d", (int)(t % 1000) + 50);

        char *args[] = {"wget", "-q", "-O", name, url, NULL};
        execv("/bin/wget", args);
    }
}

```
Fungsi `countFiles` menerima sebuah parameter berupa string yang menyimpan alamat dari sebuah folder (direktori). Fungsi ini akan membuka direktori tersebut menggunakan fungsi `opendir` yang terdapat pada library <dirent.h>. Kemudian, variabel count diinisialisasi dengan nilai 0, yang nantinya akan digunakan untuk menghitung jumlah file dalam folder.

Kemudian, fungsi akan memulai loop `while` yang berjalan selama masih terdapat file atau direktori pada folder tersebut yang belum dihitung. Setiap file atau direktori yang ditemukan akan diperiksa apakah namanya adalah "." atau ".." yang merupakan nama khusus yang digunakan untuk menunjukkan direktori itu sendiri dan parent directory masing-masing. Jika nama file atau direktori bukan "." atau "..", maka variabel count akan diincrement. Setelah loop selesai, direktori akan ditutup menggunakan fungsi `closedir`. Fungsi kemudian akan mengembalikan jumlah file dalam folder tersebut melalui nilai balik fungsi.
```c
int countFiles(char folderpath[]){
    DIR *dp;
    struct dirent *ep;
    int count = 0;
    dp = opendir(folderpath);

    if (dp != NULL) {
        while ((ep = readdir (dp))) {
            if (!strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..")) continue;
            else count++;
        }
        closedir (dp);
    }

    return count;
}
```

Berikut adalah daemon yang berjalan ketika program lukisan dijalankan dengan mode -a:
![Screenshot_2023-04-08_211957](/uploads/0dd3c69f5d5aa37932af37d60e5de6d4/Screenshot_2023-04-08_211957.png)

Berikut adalah isi program killer.c ketika program lukisan dijalankan dengan mode -a:
![Screenshot_2023-04-08_212013](/uploads/eaafcc2d2fc94a3517b1774b4de207a6/Screenshot_2023-04-08_212013.png)

Berikut adalah isi dari salah satu folder:
![Screenshot_2023-04-08_212049](/uploads/9420e1935af3539be7f29c7d11bc4dd9/Screenshot_2023-04-08_212049.png)

Berikut adalah daemon yang berjalan ketika program lukisan dijalankan dengan mode -b:
![Screenshot_2023-04-08_212609](/uploads/030deb9834fd415b227e8f6a5231eaf2/Screenshot_2023-04-08_212609.png)

Berikut adalah isi program killer.c ketika program lukisan dijalankan dengan mode -b:
![Screenshot_2023-04-08_212626](/uploads/1b9288bda93b5026f476ea7a17c511c8/Screenshot_2023-04-08_212626.png)

Berikut adalah folder-folder sesaat setelah killer dijalankan pada mode -b:
![Screenshot_2023-04-08_212714](/uploads/f99811c142fe7c2e62d8b5fc4f2486ac/Screenshot_2023-04-08_212714.png)

Berikut adalah zip-zip yang tetap terbentuk meskipun killer telah dipanggil pada mode -b:
![Screenshot_2023-04-08_212755](/uploads/be99d0d8f079070c719bd26a39804f1f/Screenshot_2023-04-08_212755.png)


## Soal 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

a. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

b. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  

c. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

d. Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

	Catatan :
	- Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
	- Tidak boleh menggunakan system()
	- Tidak boleh memakai function C mkdir() ataupun rename().
	- Gunakan exec() dan fork().
	- Directory “.” dan “..” tidak termasuk yang akan dihapus.
	- Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

### Penyelesaian
Soal ini meminta untuk dibuatkan sebuah program C yang dapat mengunduh file database pemain bola dan mengekstrak file "players.zip", menghapus semua pemain yang bukan dari Manchester United di dalam directory, menyortir pemain Manchester United sesuai dengan posisi mereka dan memindahkan setiap pemain ke dalam folder yang sesuai (Kiper, Bek, Gelandang, atau Penyerang), menemukan rating terbaik dan menampilkan formasi tim tersebut dalam sebuah file txt.

Fungsi buatTim memiliki tiga parameter input bertipe integer, yaitu bek, gel, dan str. Fungsi ini memiliki tujuan untuk membuat empat file teks dengan nama penyerang.txt, gelandang.txt, bek.txt, dan kiper.txt, yang berisi list dari file yang ada di empat folder yang berbeda, yaitu Penyerang, Gelandang, Bek, dan Kiper. Untuk melakukan tugas tersebut, fungsi buatTim menggunakan fungsi `fork()` sebanyak empat kali.
```c
void buatTim(int bek, int gel, int str){ 
    for (int i=0; i<4; i++) {
        pid_t child_id1 = fork();
        
```
Proses `fork()` tersebut akan menjalankan beberapa perintah yang ditentukan pada setiap kondisi if. Pada setiap iterasi, `fork()` akan menghasilkan dua proses yaitu parent process dan child process. Jika nilai yang dikembalikan oleh fork() kurang dari nol, maka itu menandakan terjadi kesalahan dalam pembentukan proses dan program akan keluar melalui exit(EXIT_FAILURE).
```c
if (child_id1 < 0) {
            exit(EXIT_FAILURE);
        }
```
Apabila nilai yang dikembalikan oleh `fork()` sama dengan nol, maka program sedang berjalan pada child process. Pada child process, kondisi if akan menentukan perintah yang harus dijalankan. Dalam hal ini, perintah yang dijalankan adalah `ls` yang berfungsi untuk mencetak list file yang ada di dalam folder yang telah ditentukan, misal perintah "ls Penyerang > penyerang.txt", berarti menampilkan isi direktori "Penyerang" dan menyimpan outputnya ke dalam file "penyerang.txt". Jika i bernilai 1, maka akan dieksekusi perintah "ls Gelandang > gelandang.txt", dan begitupun seterusnya. Kemudian output dari command tersebut akan disimpan ke dalam file teks yang telah disebutkan sebelumnya. Command `ls` dijalankan menggunakan /bin/sh dengan argumen `-c`, dan diberikan dalam bentuk array of string yang disimpan dalam variabel argv[].
```c
else if (child_id1 == 0) {
            if(i == 0){
                char *argv[] = {"/bin/sh", "-c", "ls Penyerang > penyerang.txt", NULL};
                execv("/bin/sh", argv);
            }
            else if(i == 1){
                char *argv[] = {"/bin/sh", "-c", "ls Gelandang > gelandang.txt", NULL};
                execv("/bin/sh", argv);
            }
            else if(i == 2){
                char *argv[] = {"/bin/sh", "-c", "ls Bek > bek.txt", NULL};
                execv("/bin/sh", argv);
            }
            else if(i == 3){
                char *argv[] = {"/bin/sh", "-c", "ls Kiper > kiper.txt", NULL};
                execv("/bin/sh", argv);
            }
        }
```
Sementara itu, pada parent process, program akan menunggu child process selesai menjalankan tasknya melalui wait(&status). Karena dalam fungsi ini terdapat empat fork() yang dijalankan secara berurutan, maka `wait()` digunakan untuk menunggu seluruh child process selesai dijalankan sebelum melanjutkan eksekusi program ke tahap selanjutnya.
```c
else {
            int status;
            while(wait(&status) > 0);
        }
    }
```
Lalu perintah `if` dan `else if` akan dijalankan oleh child process yang kedua. Pada setiap kondisi if, program akan menjalankan sorting pada file teks tertentu menggunakan perintah `sort` dari /bin/sort. `-t_` digunakan untuk menentukan delimiter yang digunakan pada file teks,`-k 3` untuk menentukan kolom ke-3 sebagai key untuk sorting, `-r` untuk mengurutkan secara reverse (descending/turun), dan `-o` "namafile" untuk menuliskan hasil sorting pada file dengan nama tertentu. Setelah itu, parent process akan menunggu child process selesai menjalankan tasknya melalui wait(&status).

Pada intinya kode ini akan menghasilkan child process yang akan melakukan sorting pada 4 file teks (penyerang.txt, gelandang.txt, bek.txt, kiper.txt) dengan opsi yang sudah ditentukan pada variabel `argv`. Hasil sorting akan dituliskan kembali pada file teks yang sama dengan nama yang sama.
```c
for (int i=0; i<4; i++) {
        pid_t child_id2 = fork();

        if (child_id2 < 0) {
            exit(EXIT_FAILURE);
        }
        else if (child_id2 == 0) {
            if(i == 0){
                char *argv[] = {"sort", "-t_", "-k", "3", "-r", "penyerang.txt", "-o", "penyerang.txt", NULL};
                execv("/bin/sort", argv);
            }
            else if(i == 1){
                char *argv[] = {"sort", "-t_", "-k", "3", "-r", "gelandang.txt", "-o", "gelandang.txt", NULL};
                execv("/bin/sort", argv);
            }
            else if(i == 2){
                char *argv[] = {"sort", "-t_", "-k", "3", "-r", "bek.txt", "-o", "bek.txt", NULL};
                execv("/bin/sort", argv);
            }
            else if(i == 3){
                char *argv[] = {"sort", "-t_", "-k", "3", "-r", "kiper.txt", "-o", "kiper.txt", NULL};
                execv("/bin/sort", argv);
            }
        }
        else {
            int status;
            while(wait(&status) > 0);
        }
    }
```
Child process selanjutnya akan menjalankan perintah-perintah untuk membentuk formasi sepak bola, yaitu mengurutkan data pemain berdasarkan posisi mereka pada tim (penyerang, gelandang, bek, kiper). Kode ini akan memeriksa nilai variabel i untuk menentukan posisi pemain mana yang sedang diproses. Misalnya, jika i == 3, artinya program memproses data penyerang, sehingga perintah digunakan untuk mengeluarkan str baris teratas dari file penyerang.txt yang telah diurutkan dan menambahkannya ke file baru. Fungsi `sprintf` digunakan untuk membuat string perintah yang berisi nama file yang benar dan jumlah baris yang akan diekstrak. Perintah kemudian dieksekusi menggunakan fungsi `execv`.

Demikian pula jika i == 2, program mengeluarkan gel baris teratas dari file gelandang.txt yang telah diurutkan, dan jika i == 1, ia mengeluarkan bek baris teratas dari file bek.txt yang telah diurutkan. Akhirnya, jika i == 0, ia hanya mengeluarkan satu baris pertama dari file kiper.txt yang telah diurutkan.

Setelah perintah dieksekusi, proses anak keluar, dan proses induk melanjutkan ke iterasi berikutnya dari loop sampai semua posisi pemain telah diproses. Hasilnya adalah file baru bernama Formasi_bek_gel_str.txt yang berisi pemain terbaik untuk setiap posisi berdasarkan formasi yang diberikan.
```c
for (int i=0; i<4; i++) {
        pid_t child_id3 = fork();

        if (child_id3 < 0) {
            exit(EXIT_FAILURE);
        }
        else if (child_id3 == 0) {
            if(i == 3){
                char command[100];
                sprintf(command, "head -n %d penyerang.txt >> /home/jagircom/Formasi_%d_%d_%d.txt", str, bek, gel, str);
                char *argv[] = {"/bin/sh", "-c", command, NULL};
                execv("/bin/sh", argv);
            }            
            else if(i == 2){
                char command[100];
                sprintf(command, "head -n %d gelandang.txt >> /home/jagircom/Formasi_%d_%d_%d.txt", gel, bek, gel, str);
                char *argv[] = {"/bin/sh", "-c", command, NULL};
                execv("/bin/sh", argv);
            }
            else if(i == 1){
                char command[100];
                sprintf(command, "head -n %d bek.txt >> /home/jagircom/Formasi_%d_%d_%d.txt", bek, bek, gel, str);
                char *argv[] = {"/bin/sh", "-c", command, NULL};
                execv("/bin/sh", argv);
            }
            else if(i == 0){
                char command[100];
                sprintf(command, "head -n 1 kiper.txt >> /home/jagircom/Formasi_%d_%d_%d.txt", bek, gel, str);
                char *argv[] = {"/bin/sh", "-c", command, NULL};
                execv("/bin/sh", argv);
            }
        }
        else {
            int status;
            while(wait(&status) > 0);
        }
    }
}
```
Selanjutnya, fork terakhir ini akan memindahkan file pemain sepakbola dari direktori "players" ke empat direktori yang berbeda, yaitu "Penyerang", "Gelandang", "Bek", dan "Kiper" menggunakan perintah `find` dan `cp`. Di dalam setiap child process, program menjalankan `find` untuk mencari file yang sesuai dengan kriteria tertentu di dalam direktori players tergantung pada nilai variabel i pada setiap iterasi loop. Jika i = 0, program akan mencari file yang memiliki nama yang mengandung string "Penyerang" dengan menggunakan opsi -name "*Penyerang*". Jika i = 1, program akan mencari file yang memiliki nama yang mengandung string "Gelandang" dengan menggunakan opsi -name "*Gelandang*". Begitu seterusnya untuk nilai i yang lain. 
Setelah menemukan file yang sesuai, program menjalankan perintah `cp` untuk menyalin file ke direktori tujuan yang sesuai dengan kriteria pencarian. Direktori tujuan juga berbeda-beda tergantung pada nilai variabel i pada setiap iterasi loop. Jika i = 2, file akan disalin ke direktori Bek. Jika i = 3, file akan disalin ke direktori Kiper, dan seterusnya untuk nilai i yang lain.
Setelah selesai menyalin file, program menunggu child process selesai dengan menggunakan `wait()` pada parent process. Hal ini dilakukan untuk memastikan bahwa semua child process selesai sebelum program berakhir.
```c
for (int i=0; i<4; i++) {
        pid_t child_id = fork();

        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }
        else if (child_id == 0) {
            if(i == 0){
                char *argv[] = {"find", "players", "-name", "*Penyerang*", "-type", "f", "-exec", "cp", "{}", "Penyerang", ";", NULL};
                execv("/bin/find", argv);
            }
            else if(i == 1){
                char *argv[] = {"find", "players", "-name", "*Gelandang*", "-type", "f", "-exec", "cp", "{}", "Gelandang", ";", NULL};
                execv("/bin/find", argv);
            }
            else if(i == 2){
                char *argv[] = {"find", "players", "-name", "*Bek*", "-type", "f", "-exec", "cp", "{}", "Bek", ";", NULL};
                execv("/bin/find", argv);
            }
            else if(i == 3){
                char *argv[] = {"find", "players", "-name", "*Kiper*", "-type", "f", "-exec", "cp", "{}", "Kiper", ";", NULL};
                execv("/bin/find", argv);
            }
        }
        else {
            int status;
            while(wait(&status) > 0);
        }
    }
}
```
Lalu dibuat sebuah fungsi `removeManUtd()` yang akan menghapus file-file yang terdapat di dalam direktori "players", kecuali file-file yang memiliki nama yang mengandung "ManUtd" dengan menggunakan perintah `find` dan `rm`. 
```c
void removeManUtd(){
    pid_t child_id = fork();
```
 Fungsi `fork()` akan membuat sebuah proses baru, dan akan mengembalikan nilai 0 pada proses child, dan nilai pid dari child pada proses parent. Jika nilai child_id < 0, maka proses fork gagal dan program akan keluar dengan kode kesalahan (EXIT_FAILURE).
```c
    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
```
Jika nilai child_id sama dengan 0, maka ini berarti kita berada dalam child process dan kita akan mengeksekusi perintah `find` dan `rm` yang akan dimasukkan ke dalam array `args`. `find` digunakan ntuk mencari file-file yang akan dihapus, pada soal ini players adalah direktori tempat file-file yang akan dihapus berada. `-type` adalah argumen yang menunjukkan tipe file yang akan dicari ("-type f"). `!` merupakan operator negasi yang digunakan untuk mengecualikan file-file yang memiliki nama yang mengandung "ManUtd". `-name` digunakan untuk menentukan pola pencarian nama file yang akan dihapus. `rm `merupakan perintah untuk menghapus file yang ditemukan oleh "find".
Setelah semua argumen didefinisikan pada `args`, dilakukan eksekusi perintah `find` dan `rm` menggunakan fungsi `execvp()` dengan menggunakan argumen yang telah didefinisikan pada `args`. Dengan begitu, `find` akan mencari semua file dalam direktori "players" yang tidak memiliki nama yang mengandung "ManUtd", dan akan menghapus file-file tersebut dengan `rm`.
```c
    else if (child_id == 0){
        char *args[] = {"find", "players", "-type", "f", "!", "-name", "*_ManUtd_*", "-exec", "rm", "{}", "+", NULL};
        execvp("find", args);
    }
    else {
        int status;
        while(wait(&status) > 0);
    }
}
```
Fungsi `removeZip()` berfungsi untuk menghapus file ZIP dengan nama "players.zip" pada direktori. Pertama, dilakukan proses forking untuk membuat sebuah child process. Jika gagal, maka program akan keluar dengan status error (EXIT_FAILURE). Lalu, dilakukan eksekusi perintah `rm` untuk menghapus file ZIP "players.zip" dengan menggunakan `argv` yang berisi argumen-argumen yang akan diberikan kepada perintah "rm" yang kemudian diikuti oleh nama file ZIP yang akan dihapus yaitu "players.zip", dan argumen terakhir adalah NULL untuk menandakan akhir dari array of string. Fungsi `execv()` digunakan untuk menjalankan perintah `rm` dengan menggunakan binary "/bin/rm". Kemudian oleh parent process dilakukan proses wait dengan menggunakan `wait()` untuk menunggu child process selesai dieksekusi dan mengambil status dari child process tersebut.
```c
void removeZip(){
    pid_t child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"rm", "players.zip", NULL};
        execv("/bin/rm", argv);
    }
    else {
        int status;
        while(wait(&status) > 0);
    }
}
```
Dibuat fungsi `extractZip()`, yang berfungsi untuk mengekstrak file ZIP dengan nama "players.zip" pada direktori saat ini. Pertama, dilakukan proses forking untuk membuat sebuah child process. Jika proses forking gagal, maka program akan keluar dengan status error (EXIT_FAILURE). Kedua, child process dijalankan  dan dilakukan eksekusi perintah `unzip` untuk mengekstrak file "players.zip". Pada baris ini, terdapat sebuah string array `argv` yang berisi argumen-argumen yang akan diberikan kepada perintah `unzip` yang di antaranya merupakan nama perintah yang akan dijalankan, kemudian diikuti oleh nama file ZIP yang akan diekstrak yaitu "players.zip", dan argumen terakhir adalah NULL untuk menandakan akhir dari array. Fungsi `execv()` digunakan untuk menjalankan perintah `unzip` dan fungsi `execv()` akan mengeksekusi `unzip` dengan menggunakan binary "/bin/unzip". 

Kemudian dilakukan proses `wait` untuk menunggu child process selesai dieksekusi dan mengambil status dari child process tersebut. Setelah child process selesai dieksekusi, akan dipanggil fungsi `removeZip()` untuk menghapus file ZIP "players.zip" dari direktori saat ini.
```c
void extractZip(){
    pid_t child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"unzip", "players.zip", NULL};
        execv("/bin/unzip", argv);
    }
    else {
        int status;
        while(wait(&status) > 0);
        removeZip();
    }
}
```
Lalu, untuk fungsi `downloadZip()` berfungsi untuk mendownload file ZIP dengan menggunakan perintah `wget` pada Google Drive. Argumen `wget` diikuti oleh argumen `-O` yang berarti output file, diikuti dengan nama file ZIP yang akan didownload yaitu "players.zip", dan terakhir adalah URL tempat file ZIP tersebut berada. Setelah child process selesai dieksekusi, akan dipanggil fungsi `extractZip()` untuk mengekstrak file ZIP yang telah didownload dengan menggunakan perintah `unzip`.
```c
void downloadZip(){
    pid_t child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"wget", "-O", "players.zip", "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
        execv("/bin/wget", argv);
    }
    else {
        int status;
        while(wait(&status) > 0);
        extractZip();
    }
}
```
Fungsi `main()` sendiri berisi kode yang akan memanggil fungsi-fungsi sebelumnya dan akan menjalankan perintah untuk mendownload file ZIP yang berisi data pemain sepak bola, mengekstraknya, menghapus data pemain dari klub Manchester United, memindahkan data pemain ke dalam folder yang sesuai dengan posisinya di lapangan, dan membentuk sebuah tim sepak bola dengan formasi 4-4-2.
```c
int main(){
    downloadZip();
    removeManUtd();
    positionFolder();
    buatTim(4, 4, 2);
    return 0;
}
```

Berikut adalah hasil extract pada poin a:
![Screenshot_2023-04-08_214805](/uploads/bf82eea748f0070d6dac68da0694b9ef/Screenshot_2023-04-08_214805.png)

Berikut adalah hasil penghapusan selain pemain Manchester United dari poin b:
![Screenshot_2023-04-08_214645](/uploads/3b010cac48d6e47cffaf8b13c8f9a74d/Screenshot_2023-04-08_214645.png)

Berikut adalah hasil folder kiper, bek, gelandang, dan penyerang dari poin c:

![Screenshot_2023-04-08_215048](/uploads/d8f3a666346ae05576b3d48277a61277/Screenshot_2023-04-08_215048.png)

![Screenshot_2023-04-08_215104](/uploads/881f663e2d3d86c6108ae12ca263fd74/Screenshot_2023-04-08_215104.png)

![Screenshot_2023-04-08_215115](/uploads/e1517ec7d51ff1ae219d96e1a178653d/Screenshot_2023-04-08_215115.png)

![Screenshot_2023-04-08_215125](/uploads/78b8d32e074bdbf50592591ba0c42d59/Screenshot_2023-04-08_215125.png)

Berikut adalah hasil formasi terbaik (4-4-2) dari poin d:
![Screenshot_2023-04-08_215233](/uploads/ce7b05aa3cb400d6827b39b04d57fc75/Screenshot_2023-04-08_215233.png)


## Soal 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:
- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
- Bonus poin apabila CPU state minimum.
Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

### Penyelesaian
Soal ini meminta kita untuk membuat sebuah program dengan beberapa ketentuan custom dan menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.

```c
bool error(int argc, char *argv[]){
```

Fungsi error pada potongan kode tersebut digunakan untuk memeriksa apakah input yang diberikan ke dalam program tersebut valid atau tidak. Fungsi ini mengembalikan nilai boolean true jika ada kesalahan pada input, dan false jika input valid. Fungsi error mengambil dua parameter, yaitu argc yang merupakan jumlah argumen pada input, dan argv yang merupakan array of strings yang berisi argumen-argumen tersebut.

Fungsi ini melakukan beberapa pengecekan pada argumen-argumen tersebut, yaitu:

1. Jika salah satu dari 4 argumen pertama kosong, maka dikembalikan nilai true (invalid).

```c
if (argv[1] == NULL || argv[2] == NULL || argv[3] == NULL || argv[4] == NULL) return true;
```
2. Loop for akan dijalankan dari i = 1 hingga i = 3 untuk memeriksa validitas tiga argumen pertama.
```c
else {
    for (int i = 1; i <= 3; i++){
```
3. Jika argumen jam, menit, atau detik (argumen pertama, kedua, atau ketiga) hanya berisi satu karakter, dan karakter tersebut bukanlah '*', atau karakter tersebut bukanlah angka di antara 0-9, maka dikembalikan nilai true (invalid).
```c
if (strlen(argv[i]) == 1 && argv[i][0] != '*' && (argv[i][0] < 48 || argv[i][0] > 59)) return true;
```
4. Pada argumen jam (i = 1), kode memeriksa apakah karakter pertama adalah '1' atau '2' (karena jam harus berada antara 0-23). Jika tidak, kode mengembalikan nilai true. Jika '1', diperiksa juga apakah karakter kedua berada di antara 0-9. Jika tidak, kode mengembalikan nilai true. Jika '2', karakter kedua akan diperiksa apakah berada di antara 0-3. Jika tidak, kode mengembalikan nilai true.
```c
if (i == 1) {
    if (argv[i][0] < 49 || argv[i][0] > 50) return true;
    else if (argv[i][0] == 49 && (argv[i][1] < 48 || argv[i][1] > 57)) return true;
    else if (argv[i][0] == 50 && (argv[i][1] < 48 || argv[i][0] > 51)) return true;
}
```

5.  Potongan kode di bawah terjadi ketika argumen jam, menit, atau detik berisi dua karakter dan validitas argumen juga akan diperiksa berdasarkan posisinya (jam, menit, atau detik).

```c
else if (strlen(argv[i]) == 2){
```
6. Pada indeks ke-1, akan dicek apakah karakter pertama berada dalam range 49 hingga 54 (representasi ASCII untuk karakter 1 hingga 6 dalam format desimal). Jika tidak, kode mengembalikan nilai true.
```c
else {
    if (argv[i][0] < 49 || argv[i][0] > 54) return true;
```
7. Karakter kedua akan dicek apakah berada dalam range 48 hingga 57 (representasi ASCII untuk karakter 0 hingga 9 dalam format desimal). Jika tidak, kode mengembalikan nilai true.
```c
else if (argv[i][1] < 48 || argv[i][1] > 57) return true;
```
8. Panjang string argumen akan dicek dan jika panjang string kurang dari 0 atau lebih dari 2, maka program akan mengembalikan nilai true.
```c
else if (strlen(argv[i]) < 0 || strlen(argv[i]) > 2) return true;
```
Jika semua pemeriksaan berhasil, kode melanjutkan ke iterasi berikutnya dari loop for untuk memeriksa argumen berikutnya.

Jika semua argumen lolos pemeriksaan validitas, kode mengembalikan nilai false, menunjukkan bahwa tidak ada kesalahan.

Lalu, fungsi di bawah ini akan memeriksa apakah argumen keempat (path file yang akan dijalankan) ada atau tidak. Jika tidak ada atau file tidak ditemukan, maka dikembalikan nilai true.
Jika tidak ada kesalahan pada input, maka dikembalikan nilai false
```c
 struct stat sb;
    int res = stat(argv[4], &sb);
    if (res) return true;
    
    return false;
}
```

Fungsi `main()` menerima dua argumen yaitu `argc` dan `*argv[]`. `argc` meninympan jumlah argumen yang diterima oleh program, termasuk nama program itu sendiri, sedangkan `*argv[]` adalah array yang berisi argumen yang diterima oleh program.

Sementara, fungsi `error()` akan memeriksa apakah input yang diberikan oleh pengguna sesuai atau tidak. Jika input yang diberikan salah, maka program akan menampilkan "ERROR: INPUT SALAH" dan return nilai 0.

```c
int main(int argc, char *argv[]){
    if(error(argc, argv)){
        printf("ERROR: INPUT SALAH\n");
        return 0;
```

Berikut adalah pesan error ketika argumen input salah:
![Screenshot_2023-04-08_213109](/uploads/71471d754247cd9241d303c899c38eda/Screenshot_2023-04-08_213109.png)

Lalu, jika `fork()` mengembalikan nilai < 0, artinya gagal membuat proses baru, maka program akan keluar dan menampilkan pesan kesalahan. Jika fork() mengembalikan nilai 0, child process akan menunggu selama 1 detik dengan fungsi `sleep(1)`, lalu memanggil program mainan kembali secara background dengan menggunakan argumen `&`.

```c
 pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){ 
        sleep(1);
        char *args[] = {argv[0], argv[1], argv[2], argv[3], argv[4], "&", NULL};
        execv(argv[0], args);    
    }
```
Pada parent process, program akan memeriksa apakah argument ke-5 kosong atau berisi argumen `&` menggunakan perintah strcmp. Jika iya, maka program akan memanggil fungsi `killMainan()` dan melakukan return 0. Pemanggilan `killMainan()` akan hanya terjadi satu kali ketika program dijalankan pertama kaili tanpa argumen `&`, setelah itu tidak akan dipanggil kembali karena pada child process program dipanggil kembali dengan argumen `&`.

Digunakan fungsi `void killMainan()` yang bertujuan untuk menghentikan proses mainan yang lain dikarenakan program bertujuan menjalankan satu cron job saja. Fungsi ini melakukan penghentian dengan menggunakan fungsi `fork()` dan `execv()`.

Fungsi `fork()` akan membuat proses baru yang akan disimpan dalam variabel child_id dan dilakukan pengecekan apakah nilainya kurang dari 0 atau tidak. Jika kurang dari 0, proses fork() gagal dan fungsi akan berhenti dengan menggunakan fungsi `exit()` dan return nilai EXIT_FAILURE.
```c
void killMainan(){
    pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
```
Jika nilai child_id = 0, maka proses ini adalah child process yang akan menghentikan program mainan menggunakan fungsi `execv()` untuk menjalankan `pkill` dengan argumen -f ./mainan.*. Perintah `pkill` akan mencari dan menghentikan semua proses dengan nama yang sama dengan mainan. Argumen `-f` digunakan untuk menghentikan semua proses yang memiliki nama yang sama. Sedangkan argumen `&` digunakan untuk menjalankan perintah di latar belakang (background process).
Jika nilai child_id bukan kurang dari 0 dan bukan sama dengan 0, artinya proses ini adalah parent process dan akan melanjutkan kode setelah pemanggilan fungsi killMainan().

```c
    else if (child_id == 0){ 
        char *args[] = {"pkill", "-f", "./mainan.*&", NULL};
        execv("/bin/pkill", args);    
    }
}
```

Selanjutnya, program akan memperoleh waktu saat ini menggunakan fungsi `time(NULL)` dan menyimpannya pada variabel t. Kemudian, waktu saat ini dikonversi menjadi local time menggunakan struct tm *current = localtime(&t.

Program akan memeriksa apakah waktu saat ini sesuai dengan argument waktu yang diberikan oleh pengguna. Jika ya, program akan mempersiapkan perintah yang akan dijalankan menggunakan array args yang terdiri dari bash dan argument perintah yang diberikan oleh pengguna. Setelah itu, program akan mengeksekusi perintah menggunakan fungsi `execv`. Hal ini menunjukkan bahwa program akan mengeksekusi perintah pada waktu yang ditentukan dan argument ke-5 adalah "&" atau kosong.

Jika waktu saat ini tidak sesuai dengan argumen waktu yang diberikan pengguna, maka program tidak akan keluar dan melakukan apapun.
```c
else {
        if (!argv[5] || strcmp(argv[5], "&")){
            killMainan();
            return 0;
        }
        time_t t = time(NULL);
        struct tm *current = localtime(&t);

        if ((!strcmp(argv[1], "*") || atoi(argv[1]) == current->tm_hour) &&
            (!strcmp(argv[2], "*") || atoi(argv[2]) == current->tm_min) &&
            (!strcmp(argv[3], "*") || atoi(argv[3]) == current->tm_sec)){
            char *args[] = {"bash",  argv[4], NULL};
            execv("/bin/bash", args);
        }      
    }
}
```

Berikut adalah salah satu contoh isi file programcron.sh:
![Screenshot_2023-04-08_213000](/uploads/5e796578e007c524bd52bcc54bf8de68/Screenshot_2023-04-08_213000.png)

Berikut adalah output programcron.sh jika argumen input benar dan juga proses daemonnya:
![Screenshot_2023-04-08_214054](/uploads/a7a3f85bbb015a88615962d86c448b5a/Screenshot_2023-04-08_214054.png)
