#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

bool error(int argc, char *argv[]){
    if (argv[1] == NULL || argv[2] == NULL || argv[3] == NULL || argv[4] == NULL) return true;
    else {
        for (int i = 1; i <= 3; i++){
            if (strlen(argv[i]) == 1 && argv[i][0] != '*' && (argv[i][0] < 48 || argv[i][0] > 59)) return true;
            else if (strlen(argv[i]) == 2){
                if (i == 1){
                    if (argv[i][0] < 49 || argv[i][0] > 50) return true;
                    else if (argv[i][0] == 49 && (argv[i][1] < 48 || argv[i][1] > 57)) return true;
                    else if (argv[i][0] == 50 && (argv[i][1] < 48 || argv[i][0] > 51)) return true;
                }
                else {
                    if (argv[i][0] < 49 || argv[i][0] > 54) return true;
                    else if (argv[i][1] < 48 || argv[i][1] > 57) return true;
                }
            }
            else if (strlen(argv[i]) < 0 || strlen(argv[i]) > 2) return true;
        }
    }
    
    struct stat sb;
    int res = stat(argv[4], &sb);
    if (res) return true;
    
    return false;
}

void killMainan(){
    pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){ 
        char *args[] = {"pkill", "-f", "./mainan.*&", NULL};
        execv("/bin/pkill", args);    
    }
}

int main(int argc, char *argv[]){
    if(error(argc, argv)){
        printf("ERROR: INPUT SALAH\n");
        return 0;
    }

    pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){ 
        sleep(1);
        char *args[] = {argv[0], argv[1], argv[2], argv[3], argv[4], "&", NULL};
        execv(argv[0], args);    
    }
    else {
        if (!argv[5] || strcmp(argv[5], "&")){
            killMainan();
            return 0;
        }
        time_t t = time(NULL);
        struct tm *current = localtime(&t);

        if ((!strcmp(argv[1], "*") || atoi(argv[1]) == current->tm_hour) &&
            (!strcmp(argv[2], "*") || atoi(argv[2]) == current->tm_min) &&
            (!strcmp(argv[3], "*") || atoi(argv[3]) == current->tm_sec)){
            char *args[] = {"bash",  argv[4], NULL};
            execv("/bin/bash", args);
        }      
    }
}