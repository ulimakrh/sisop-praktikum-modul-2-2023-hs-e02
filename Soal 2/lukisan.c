#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <wait.h>

void compileKiller(){
    pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *args[] = {"gcc", "/home/razanathallah/Documents/SISOP/M2/killer.c", "-o", "/home/razanathallah/Documents/SISOP/M2/killer", NULL};
        execv("/bin/gcc", args);
    }
}

void zipFolder(char path[], char name[]){
    pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        chdir(path);
        char *args[] = {"zip", "-r", "-m", name, name, NULL};
        execv("/bin/zip", args);
    }
}

void downloadImage(char folderpath[]){
    pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char name[400], url[100];
        time_t t = time(NULL);
        struct tm *current = localtime(&t);

        sprintf(name, "%s/%04d-%02d-%02d_%02d:%02d:%02d", folderpath, current->tm_year + 1900, 
                current->tm_mon + 1, current->tm_mday, current->tm_hour, current->tm_min, current->tm_sec);  
        sprintf(url, "https://picsum.photos/%d", (int)(t % 1000) + 50);

        char *args[] = {"wget", "-q", "-O", name, url, NULL};
        execv("/bin/wget", args);
    }
}

int countFiles(char folderpath[]){
    DIR *dp;
    struct dirent *ep;
    int count = 0;
    dp = opendir(folderpath);

    if (dp != NULL) {
        while ((ep = readdir (dp))) {
            if (!strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..")) continue;
            else count++;
        }
        closedir (dp);
    }

    return count;
}

void findFolder() {
    pid_t pid = fork(), sid;
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        DIR *dp;
        struct dirent *ep;
        char *path = "/home/razanathallah/Documents/SISOP/M2";
        dp = opendir(path);
        struct stat sb;

        if (dp != NULL) {
            while ((ep = readdir (dp))) {
                if (!strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..")) continue;

                char folderpath[300];
                sprintf(folderpath, "%s/%s", path, ep->d_name);
                if (stat(folderpath, &sb) == -1) continue;

                if (S_ISDIR(sb.st_mode)){
                    int count = countFiles(folderpath);
                    if (count < 15) downloadImage(folderpath);
                    else zipFolder(path, ep->d_name);
                }
            }
            closedir (dp);
        }
        sleep(5);
    }
}

void createFolder(){
    pid_t pid = fork();
    if (pid < 0){
        exit(EXIT_FAILURE);
    }
    else if (pid == 0){
        char name[120];
        time_t t = time(NULL);
        struct tm *current = localtime(&t);
        sprintf(name, "/home/razanathallah/Documents/SISOP/M2/%04d-%02d-%02d_%02d:%02d:%02d", current->tm_year + 1900, 
                current->tm_mon + 1, current->tm_mday, current->tm_hour, current->tm_min, current->tm_sec);  
        char *args[] = {"mkdir", name, NULL};
        execv("/bin/mkdir", args);
    }
}

void createKiller(char *argv[], int pid){
    if (!strcmp(argv[1], "-a")){
        FILE *fp;
        fp = fopen("/home/razanathallah/Documents/SISOP/M2/killer.c", "w");
        
        fprintf(fp, 
            "#include <stdlib.h>\n"
            "#include <sys/types.h>\n"
            "#include <unistd.h>\n"
            "#include <stdio.h>\n"
            "#include <wait.h>\n"

            "void removeKiller(){\n"
                "pid_t child_id = fork();\n"
                "if (child_id < 0){\n"
                    "exit(EXIT_FAILURE);\n"
                "}\n"
                "else if (child_id == 0){\n"
                    "char *args[] = {\"rm\", \"killer.c\", NULL};\n"
                    "execv(\"/bin/rm\", args);\n"
                "}\n"
                "else {\n"
                    "char *args[] = {\"rm\", \"killer\", NULL};\n"
                    "execv(\"/bin/rm\", args);\n"
                "}\n"
            "}\n"

            "int main(){\n"
                "pid_t child_id = fork();\n"
                "if (child_id < 0){\n"
                    "exit(EXIT_FAILURE);\n"
                "}\n"
                "else if (child_id == 0){\n"
                    "char *args[] = {\"pkill\", \"lukisan\", NULL};\n"
                    "execv(\"/bin/pkill\", args);\n"
                "}\n"
                "else {\n"
                    "removeKiller();\n"
                "}\n"
            "}\n"
        );
        fclose(fp);
        compileKiller();
    }
    else if (!strcmp(argv[1], "-b")){
        FILE *fp;
        fp = fopen("/home/razanathallah/Documents/SISOP/M2/killer.c", "w");
        
        fprintf(fp, 
            "#include <stdlib.h>\n"
            "#include <sys/types.h>\n"
            "#include <unistd.h>\n"
            "#include <stdio.h>\n"
            "#include <wait.h>\n"

            "void removeKiller(){\n"
                "pid_t child_id = fork();\n"
                "if (child_id < 0){\n"
                    "exit(EXIT_FAILURE);\n"
                "}\n"
                "else if (child_id == 0){\n"
                    "char *args[] = {\"rm\", \"killer.c\", NULL};\n"
                    "execv(\"/bin/rm\", args);\n"
                "}\n"
                "else {\n"
                    "char *args[] = {\"rm\", \"killer\", NULL};\n"
                    "execv(\"/bin/rm\", args);\n"
                "}\n"
            "}\n"

            "int main(){\n"
                "pid_t child_id = fork();\n"
                "if (child_id < 0){\n"
                    "exit(EXIT_FAILURE);\n"
                "}\n"
                "else if (child_id == 0){\n"
                    "char strpid[10];\n"
                    "sprintf(strpid, \"%%d\", %d);\n"
                    "char *args[] = {\"kill\", \"-9\", strpid, NULL};\n"
                    "execv(\"/bin/kill\", args);\n"
                "}\n"
                "else {\n"
                    "removeKiller();\n"
                "}\n"
            "}\n",
            pid
        );
        fclose(fp);
        compileKiller();
    }
}

void createKillerAndFolder(char *argv[]) {
    pid_t pid = fork(), sid;
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    createKiller(argv, getpid());
    while (1) {
        createFolder();
        sleep(30);
    }
}

int main(int argc, char *argv[]) {
    pid_t pid1, pid2;

    pid1 = fork();
    if (pid1 == 0) {
        createKillerAndFolder(argv);
    } else if (pid1 < 0) {
        exit(EXIT_FAILURE);
    }

    pid2 = fork();
    if (pid2 == 0) {
        findFolder();
    } else if (pid2 < 0) {
        exit(EXIT_FAILURE);
    }

    return 0;
}
